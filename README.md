# php-test desenvolvedor pleno

# Objetivo
O teste visa alinhar se o conhecimento do candidato está dentro da espectativa para a posição.

# Por onde começar?

* De um fork do projeto para a sua conta do bitbucket.
* Adicione uma branch test-dev
* O projeto consiste na criação de duas aplicações (wordpress e api) realize o teste de wordpress na pasta wp-test e a api na api-test
* Quando finalizar o teste envie o link do seu repositório para fernando.alexandre@zebradigital.com.br com o assunto: Test Dev Php

# Wordpress Test

Utilizamos muitooo wordpress na agência por isso é muito importante o conhecimento do CMS, essa etapa tem o objetivo entender se o candidato tem familiaridade com a ferramenta.

Escopo - Criar a estrutura para backend com wordpress para um site institucional, deve-se ser levado em conta no desenvolvimento do projeto que todos os conteúdos devem ser administráveis via painel.

Utilizar o seguinte tema bootstrap como referência, para áreas do site: https://startbootstrap.com/themes/business-casual/

No tema disponibilizado acima a navegação acontece via ajax, trocar para navegação por páginas, exemplo: o link "about" deve levar para algo como "http://minhaaplicacao.com.br/about"

Sinta-se livre para utilizar plugins =)

# Desenvolvimento com framework

Aqui a ideia é validar os conhecimentos no ecosistema da linguagem com o uso de algum framework de mercado, sinta-se livre para utilizar o de sua preferência ou então realizar utilizando apenas recursos da linguagem.

Escopo - Criar uma api com dois endpoint get que retorne um JSON de vídeos ou vídeo por id.

O endpoint vídeos deve ser: http://APLICACACAO_URL/videos

O endpoint vídeo deve ser: http://APLICACACAO_URL/videos/ID_DO_VIDEO

Mock json:

{
  "videos": {
    "1":{
      "titulo":"titulo",
      "descricao":"descricao",
      "imagem":"imagem",
      "youtube":"youtube",
    },
    "2":{
      "titulo":"titulo",
      "descricao":"descricao",
      "imagem":"imagem",
      "youtube":"youtube",
    },
    "3":{
      "titulo":"titulo",
      "descricao":"descricao",
      "imagem":"imagem",
      "youtube":"youtube",
    }
  }
  
Requesições para endpoints diferentes dos listados devem retornar 404.

Requisições para o endpoint de vídeo por id cujo este não exista devem retornar 404 com a mensagem vídeo não encontrado.
  
# Documentação
  
Acreditamos que não existe código certo ou errado, afinal desenvolver é uma arte e existem diversas maneiras de se chegar em uma solução, então para podermos avaliar seu código da melhor forma adicione um arquivo de documentação no projeto explicando a sua solução.
  
# Passos para rodar os projetos
  
Também adicione um arquivo explicando os passos para rodar as aplicações desenvolvidas. 

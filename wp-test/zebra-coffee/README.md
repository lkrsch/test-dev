# Teste WordPress

### Acesso
O site está publicado no domínio do meu site/portfolio
http://laurokirsch.com/wp-test

As credenciais para acesso ao painel do WordPress são
U: zebrateste
S: 09fwejlwj09e

### Documentação
- Arquivos do tema "zebra-coffee" disponíveis
- Parti de um tema em branco para fazer as adaptações
- Adaptei o template do Start Bootstrap ao tema
- Notei que um padrão que se repetia nas páginas eram títulos e subtítulos
- Criei um template de página para cada página, já que elas eram diferentes entre si
- Haviam também campos a serem editados, como os horários de abertura
- Instalei o plugin Advanced Custom Fields para edição desses campos
- Linkei os menus superiores para suas respectivas páginas
- Optei por criar dois campos de heading ao invés de criar um só e usar o outro como the_title() pra facilitar a identificação das páginas no painel do WordPress. Senão algumas páginas ficariam se chamando "WORTH DRINKING" ou algo do tipo, ao invés de "Home", "About", etc

# Home
Superior
- Template de página page-home.php criado e setado para ser a frontpage nas configurações do WordPress
- No template são chamados os custom fields que resultam em "FRESH COFFEE" e "WORTH DRINKING"
- O texto do quadrado é o the_content();
- Img: get_the_post_thumbnail();
- Botão linkando pra aba store

Inferior
- Semelhante ao superior, utilizei o padrão e chamei os fields que resultam em "OUR PROMISE" e "TO YOU"
- Importante deixar esses campos editáveis pois o cliente pode querer mudar essas chamadas e terá total autonomia pra isso

# About
- Template page-about.php
- Img: get_the_post_thumbnail();
- 2 custom fields de heading
- the_content();

# Products
- Template page-products.php
- Achei parecido com uma listagem, então me pareceu uma boa oportunidade pra criar um loop que busca posts e os lista
- Os 3 itens estão publicados em "Posts", cada um com os dois custom fields de heading + o post thumbnail + content
- Intervençãozinha no meio do loop pra adicionar classe pra mandar textos pra esquerda ou pra direita dependendo da iteração


# Store
- Template page-store.php
- 7 custom fields, um pra cada dia da semana, podendo ser informado o horário de abertura
- Para fins de praticidade coloquei os campos de puro texto, mas poderia ser utilizado um campo de data ou algum date picker para o usuário não ter que redigir os horários na mão
- + 3 custom fields de endereço, cidade e telefone
- Template chama ao fundo o post do about, fazendo um loop que puxa a página pelo nome/slug (pode ser ID também)
<?php /* Template Name: About */ ?>
<?php get_header(); ?>


		<?php if (have_posts()): while (have_posts()) : the_post(); ?>


			<section class="page-section about-heading">
				<div class="container">
					<img class="img-fluid rounded about-heading-img mb-3 mb-lg-0" src="<?php the_post_thumbnail_url(); ?>" alt="">
					<div class="about-heading-content">
						<div class="row">
							<div class="col-xl-9 col-lg-10 mx-auto">
								<div class="bg-faded rounded p-5">
									<h2 class="section-heading mb-4">
										<span class="section-heading-upper"><?php the_field("heading"); ?></span>
										<span class="section-heading-lower"><?php the_field("heading2"); ?></span>
									</h2>
									<p><?php the_content(); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>			


		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sem publicações', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

<?php get_footer(); ?>
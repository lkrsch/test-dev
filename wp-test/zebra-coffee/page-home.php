<?php /* Template Name: Home */ ?>
<?php get_header(); ?>


		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<section class="page-section clearfix">
				<div class="container">
					<div class="intro">
						<img class="intro-img img-fluid mb-3 mb-lg-0 rounded" src="<?php the_post_thumbnail_url(); ?>" alt="">
						<div class="intro-text left-0 text-center bg-faded p-5 rounded">
							<h2 class="section-heading mb-4">
								<span class="section-heading-upper"><?php the_field("heading"); ?></span>
								<span class="section-heading-lower"><?php the_field("heading2"); ?></span>
							</h2>
							<p class="mb-3">
								<?php the_content(); ?>
							</p>
							<div class="intro-button mx-auto">
								<a class="btn btn-primary btn-xl" href="<?php echo site_url(); ?>/store">Visit Us Today!</a>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="page-section cta">
				<div class="container">
					<div class="row">
						<div class="col-xl-9 mx-auto">
							<div class="cta-inner text-center rounded">
								<h2 class="section-heading mb-4">
									<span class="section-heading-upper"><?php the_field("heading-promise"); ?></span>
									<span class="section-heading-lower"><?php the_field("heading-promise2") ?></span>
								</h2>
								<p class="mb-0">
									<?php the_field("the-content-promise"); ?>
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sem publicações', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

<?php get_footer(); ?>
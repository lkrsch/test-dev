<?php /* Template Name: Products */ ?>
<?php get_header(); ?>

		<?php

			$args = array( 'post_type' => 'post', 'posts_per_page' => 3);
			$wp_query = new WP_Query($args); 
			$floatRight = true;

		?>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>


		<section class="page-section">
		    <div class="container">
		      <div class="product-item">
		        <div class="product-item-title d-flex">
		          
					<?php if ($floatRight == true) { 
						$varMargin = "ml-auto";

						// Atualiza a variável para o próximo loop
						$floatRight = false;

					} else {
						$varMargin = "mr-auto";

						// Atualiza a variável para o próximo loop
						$floatRight = true;
					} ?>

		          <div class="bg-faded p-5 d-flex <?php echo $varMargin; ?> rounded">        	


		            <h2 class="section-heading mb-0">
		              <span class="section-heading-upper"><?php the_field("heading"); ?></span>
		              <span class="section-heading-lower"><?php the_field("heading2"); ?></span>
		            </h2>
		          </div>
		        </div>
		        <img class="product-item-img mx-auto d-flex rounded img-fluid mb-3 mb-lg-0" src="<?php the_post_thumbnail_url(); ?>" alt="">

				<div class="product-item-description d-flex <?php echo $varMargin ?>">        
		          <div class="bg-faded p-5 rounded">
		            <p class="mb-0"><?php the_content(); ?></p>
		          </div>
		        </div>
		      </div>
		    </div>
		</section>


		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sem publicações', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

<?php get_footer(); ?>
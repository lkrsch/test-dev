<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<!-- Bootstrap core CSS -->
		<link href="<?php echo get_template_directory_uri(); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom fonts for this template -->
		<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">


		<!-- Custom CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/business-casual.css">

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>

		<h1 class="site-heading text-center text-white d-none d-lg-block">
		<span class="site-heading-upper text-primary mb-3">A Free Bootstrap 4 Business Theme</span>
		<span class="site-heading-lower">Business Casual</span>
		</h1>

		<!-- Navigation -->
		<nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
		<div class="container">
		  <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Start Bootstrap</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarResponsive">
		    <ul class="navbar-nav mx-auto">
		      <li class="nav-item active px-lg-4">
		        <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url(); ?>">Home
		          <span class="sr-only">(current)</span>
		        </a>
		      </li>
		      <li class="nav-item px-lg-4">
		        <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url(); ?>/about">About</a>
		      </li>
		      <li class="nav-item px-lg-4">
		        <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url(); ?>/products">Products</a>
		      </li>
		      <li class="nav-item px-lg-4">
		        <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url(); ?>/store">Store</a>
		      </li>
		    </ul>
		  </div>
		</div>
		</nav>

<?php /* Template Name: Store */ ?>
<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>


	<section class="page-section cta">
		<div class="container">
			<div class="row">
				<div class="col-xl-9 mx-auto">
					<div class="cta-inner text-center rounded">
						<h2 class="section-heading mb-5">
							<span class="section-heading-upper">Come On In</span>
							<span class="section-heading-lower">We're Open</span>
						</h2>
						<ul class="list-unstyled list-hours mb-5 text-left mx-auto">
							<li class="list-unstyled-item list-hours-item d-flex">
								Sunday
								<span class="ml-auto"><?php the_field("sunday-opening"); ?></span>
							</li>
							<li class="list-unstyled-item list-hours-item d-flex">
								Monday
								<span class="ml-auto"><?php the_field("monday-opening"); ?></span>
							</li>
							<li class="list-unstyled-item list-hours-item d-flex">
								Tuesday
								<span class="ml-auto"><?php the_field("tuesday-opening"); ?></span>
							</li>
							<li class="list-unstyled-item list-hours-item d-flex">
								Wednesday
								<span class="ml-auto"><?php the_field("wednesday-opening"); ?></span>
							</li>
							<li class="list-unstyled-item list-hours-item d-flex">
								Thursday
								<span class="ml-auto"><?php the_field("thursday-opening"); ?></span>
							</li>
							<li class="list-unstyled-item list-hours-item d-flex">
								Friday
								<span class="ml-auto"><?php the_field("friday-opening"); ?></span>
							</li>
							<li class="list-unstyled-item list-hours-item d-flex">
								Saturday
								<span class="ml-auto"><?php the_field("saturday-opening"); ?></span>
							</li>
						</ul>
						<p class="address mb-5">
							<em>
								<strong><?php the_field("street"); ?></strong>
								<br>
								<?php the_field("city"); ?>
							</em>
						</p>
						<p class="mb-0">
							<small>
								<em>Call Anytime</em>
							</small>
							<br>
							<?php the_field("phone") ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>


<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else: ?>

	<!-- article -->
	<article>

		<h2><?php _e( 'Sem publicações', 'html5blank' ); ?></h2>

	</article>
	<!-- /article -->

<?php endif; ?>


<?php

$args = array( 'post_type' => 'page', 'posts_per_page' => 1, 'pagename' => "about");
$wp_query = new WP_Query($args); 

?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<section class="page-section about-heading">
		<div class="container">
			<img class="img-fluid rounded about-heading-img mb-3 mb-lg-0" src="<?php the_post_thumbnail_url(); ?>" alt="">
			<div class="about-heading-content">
				<div class="row">
					<div class="col-xl-9 col-lg-10 mx-auto">
						<div class="bg-faded rounded p-5">
							<h2 class="section-heading mb-4">
								<span class="section-heading-upper"><?php the_field("heading"); ?></span>
								<span class="section-heading-lower"><?php the_field("heading2"); ?></span>
							</h2>
							<p><?php the_content(); ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>	

<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>
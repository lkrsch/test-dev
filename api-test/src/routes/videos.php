<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

// Todos os vídeos
$app->get('/api/videos', function(Request $request, Response $response){
    $sql = "SELECT * FROM videos";

    try {
        // Conexão
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $videos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($videos);
    } catch(PDOException $e){
        echo '{"erro": {"msg": '.$e->getMessage().'}';
    }
});

// Pegar vídeo por ID
$app->get('/api/video/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');

    $sql = "SELECT * FROM videos WHERE id = $id";

    try{
        // Conexão
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $video = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        if ($video === false) {
            echo "404 Vídeo não encontrado";
        } else {
            echo json_encode($video);
        }

    } catch(PDOException $e){
        echo '{"erro": {"msg": '.$e->getMessage().'}';
    }
});
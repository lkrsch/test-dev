-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: May 29, 2019 at 03:03 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `zebra_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `titulo`, `descricao`, `imagem`, `youtube`) VALUES
(1, 'Every Liverpool Champions League goal on the road to Madrid 2019', 'Watch every goal that helped the Reds reach a second successive UEFA Champions League final, where they prepare to take on Tottenham Hotspur at the Estadio Metropolitano in Madrid.\r\n', 'https://i.ytimg.com/vi/6feN4MNP_S4/0.jpg', 'https://www.youtube.com/watch?v=6feN4MNP_S4'),
(2, 'OnePlus 7 Pro Review: Silly Fast!', 'OnePlus 7 Pro is crazy fast in all the right ways. For $669. Nice.', 'https://i.ytimg.com/vi/PVWLD3064Ng/0.jpg', 'https://www.youtube.com/watch?v=PVWLD3064Ng'),
(3, 'Steve Jobs introduces iPhone in 2007', 'This is the iPhone introduction excerpt from the Macworld San Francisco 2007 Keynote Address January 9th, 2007. Steve Jobs made the claim that it was 5 years ahead of any other phone. AppleTV was also introduced that day.', 'https://i.ytimg.com/vi/MnrJzXM7a6o/0.jpg', 'https://www.youtube.com/watch?v=MnrJzXM7a6o'),
(4, 'Google Pixel 3a Review: A for Ace!', 'Pixel 3a. A for Affordable. A for Average. A for Acceptable. A for Ace camera.', 'https://i.ytimg.com/vi/XnSqlX1kCQo/0.jpg', 'https://www.youtube.com/watch?v=XnSqlX1kCQo'),
(5, 'Amazing Laptops Coming Soon!', 'Thoughts on the Asus ZenBook Pro Duo, the new Alienware m15 / m17 and the Razer Studio Laptops.', 'https://i.ytimg.com/vi/9Cor70qmThU/0.jpg', 'https://www.youtube.com/watch?v=9Cor70qmThU');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

# Desenvolvimento com framework

RESTful API construída com SlimPHP e MySQL

### Sem Instalação
Acessar os endpoints da aplicação em:
http://laurokirsch.com/api-test/public/api/videos

http://laurokirsch.com/api-test/public/api/video/1


### Com Instalação

Importar o BD localizado em _sql/zebra_api.sql


Editar as configurações de conexão com o banco em db/config

Instalar o SlimPHP e as dependências

```sh
$ composer install
```

### Documentação
Nessa tarefa a solução se deu seguindo princípios análogos de CRUDs com PHP + MySQL. Após criação do banco e de inserir dados fictícios na tabela, bastou criar a conexão com o banco e as funções GET com as rotas para os endpoints seguindo as diretrizes do SlimPHP, que é um framework que auxilia na criação de APIs.

Existem 5 vídeos cadastrados de IDs 1 a 5.